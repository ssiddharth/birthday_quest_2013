all:
	cd src; make > /dev/null
	sudo insmod src/bq.ko

clean:
	sudo rmmod bq
	cd src; make clean > /dev/null
