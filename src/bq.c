#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/unistd.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/atomic.h>
#include <linux/delay.h>

static unsigned long **sys_call_table;

static char* source_list[] = 
			{
				"happy birthday",
				"happy b'day",
				"happy bday",
				"hapy birthday",
				"hapy b'day",
				"hapy bday",
				"happie birthday",
				"happie b'day",
				"happie bday",
				"happy returns",
				"hapy returns",
				"happie returns",
			};

static char* fail_list [] =
			{
				" Thank You!:) ",
				" Thank You ",
				" Thank U! ",
				" ThankYou!:) ",
				" Thank U! ",
				" Thank U ",
				" Thank You! :) ",
				" Thank You! ",
				" ThankYou! ",
				" ThankYou!:) ",
				" Thank You! ",
				" ThankYou -PA ",
			};

static char* try_list [] =
			{
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
				" Thank You!! :) -PioneerAxon ",
			};

static const int replace_count = (sizeof (source_list) / sizeof (char*));

static atomic_t counter;

static void fix_case (char* str, int n)
{
	int l;
	for (l = 0; l < n; l++)
	{
		if (str [l] >= 'A' && str [l] <= 'Z')
			str [l] = str[l] - 'A' + 'a';
	}
	str[n] = '\0';
}

static void move_buffer (char* source, char* user, unsigned int start, int len_diff, unsigned int size)
{
	if (len_diff > 0)
	{
		copy_from_user (source, user, size);
		copy_to_user (user + start + len_diff, source + start, size - start);
		copy_from_user (source, user, size + len_diff);
	}
	if (len_diff < 0)
	{
		len_diff = 0 - len_diff;
		copy_from_user (source, user, size);
		copy_to_user (user + start, source + start + len_diff, size - start - len_diff);
		copy_from_user (source, user, size - len_diff);
	}
}

static int replace_all (char* source, char* user, unsigned int size, unsigned int count)
{
	int l;
	int diff;
	int len_diff;
	char* tmp;
	fix_case (source, size);
	for (l = 0; l < replace_count; l++)
	{
		tmp = source;
		len_diff = strlen (try_list [l]) - strlen (source_list [l]);
		while ((tmp = strstr (tmp, source_list [l])) != NULL)
		{
			diff = tmp - source;
			if ((int)len_diff < (int)count - (int)size)
			{
				move_buffer (source, user, diff, len_diff, size);
				if (len_diff > 0)
					size += len_diff;
				else
					size -= (unsigned int)((int)0 - (int)len_diff);
				fix_case (source, size);
				copy_to_user (user + diff, try_list [l], strlen (try_list [l]));
				tmp += len_diff + 1;
			}
			else
			{
				copy_to_user (user + diff, fail_list [l], strlen (fail_list [l]));
				tmp++;
			}
		}
	}
	return size;
}

asmlinkage long (*ref_sys_read) (unsigned int fd, char* buf, unsigned int count);

asmlinkage long new_sys_read (unsigned int fd, char* buf, unsigned int count)
{
	long ret;
	char* tmp;
	atomic_inc (&counter);
	ret = (*ref_sys_read) (fd, buf, count);
	if (count > 0 && ret > 0)
	{
		tmp = kmalloc (count + 1, GFP_KERNEL);
		if (!tmp)
		{
			atomic_dec (&counter);
			return ret;
		}
		tmp [count] = '\0';
		copy_from_user (tmp, buf, ret);
		ret = replace_all (tmp, buf, ret, count);
		kfree (tmp);
	}
	atomic_dec (&counter);
	return ret;
}

static unsigned long **aquire_sys_call_table (void)
{
	unsigned long int offset = PAGE_OFFSET;
	unsigned long **sct;
	while (offset < ULLONG_MAX)
	{
		sct = (unsigned long **)offset;
		if (sct [__NR_close] == (unsigned long *) sys_close)
			return sct;
		offset += sizeof (void *);
	}
	return NULL;
}

static void disable_page_protection (void)
{
	unsigned long value;
	asm volatile("mov %%cr0, %0" : "=r" (value));
	if (!(value & 0x00010000))
		return;
	asm volatile ("mov %0, %%cr0" : : "r" (value & ~0x00010000));
}

static void enable_page_protection (void)
{
	unsigned long value;
	asm volatile ("mov %%cr0, %0" : "=r" (value));
	if ((value & 0x00010000))
		return;
	asm volatile ("mov %0, %%cr0" : : "r" (value | 0x00010000));
}

static int __init bq2013_start (void)
{
	if (!(sys_call_table = aquire_sys_call_table ()))
		return -1;
	
	atomic_set (&counter, 0);

	disable_page_protection ();

	ref_sys_read = (void *) sys_call_table [__NR_read];
	sys_call_table [__NR_read] = (unsigned long *) new_sys_read;

	enable_page_protection ();

	return 0;
}

static void __exit bq2013_end (void)
{
	if (!sys_call_table)
		return;

	disable_page_protection ();

		sys_call_table [__NR_read] = (unsigned long *) ref_sys_read;

	enable_page_protection ();

	msleep (1000);

	while (atomic_read (&counter) > 0)
	{
		msleep (100);
	}
}

module_init (bq2013_start);
module_exit (bq2013_end);
